"use client"

import Link from "next/link";
import BackButton from "@/components/UI/BackButton";
import Input from "@/components/UI/Input";
import {useState} from "react";

export default function Card() {
	const [card, setCard] = useState('')
	const [expire, setExpire] = useState('')
	const [cvc, setCvc] = useState('')

	return(
			<div className='light'>
				<div className="container">
					<section className='section'>
						<div className='section-top'>
							<div className="page-title-wrapper">
								<BackButton href='/payment' />
								<p className='page-title'>Card</p>
							</div>
							<h1 className='big-title'>You donate <strong>$10</strong> <br/> to Alex Rodrigues</h1>
							<div className="form-control">
								<Input value={card} setValue={setCard} label='Card'/>
								<Input value={expire} setValue={setExpire} label='Expire'/>
								<Input value={cvc} setValue={setCvc} label='CVC'/>
							</div>
						</div>
						<div className='section-action'>
							<Link href='/success' className='btn btn-light'>
								Confirm Donation
							</Link>
						</div>
					</section>
				</div>
			</div>
	)
}