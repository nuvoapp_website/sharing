"use client"

import {useEffect} from "react";
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from "react-redux";
import {postUser} from "@/store/features/userSlice";

import Avatar from "@/components/UI/Avatar";
import PaymentMethod from "@/components/PaymentMethod";
import useRedirect from "@/hooks/useRedirect";
import LangButtons from "@/components/LangButtons";


export default function Home() {
  useRedirect()
  const { t } = useTranslation();
  const {user, status} = useSelector((state) => state.user)
  const dispatch = useDispatch();

  useEffect(() => {
    if (status === 'idle'){
      dispatch(postUser())
    }
  }, [dispatch]);

  return (
    <div className='home'>
      <div className="container">
        <section className='section'>
          <div className='section-top'>
            <div className="page-title-wrapper">
              <p className='page-title'>{ t('lang:donateTo') }</p>
              <LangButtons/>
            </div>
            <div className="home-avatar-wrapper">
              <Avatar user={user.user}/>
            </div>
            <h2 className='big-title'>{ t('lang:title') }</h2>
            <PaymentMethod/>
          </div>
        </section>
      </div>
    </div>
  );
}