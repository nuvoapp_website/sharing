import {Providers} from "@/store/provider";
import { Figtree } from "next/font/google";
import TranslationsProvider from "@/components/TranslationsProvider";
import initTranslations from "@/app/i18n";
import "normalize.css";
import "@/style/globals.css";

const i18nNamespaces = ['lang'];
const figtree = Figtree({ subsets: ["latin"] });

export default async function RootLayout( {children, params: {locale}} ) {
  const {t, resources} = await initTranslations(locale, i18nNamespaces);
  return (
      <TranslationsProvider namespaces={i18nNamespaces} locale={locale} resources={resources}>
        <html lang="en">
        <body className={figtree.className}>
          <Providers>
            {children}
          </Providers>
        </body>
        </html>
      </TranslationsProvider>
  );
}
