"use client"

import {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {postUser} from "@/store/features/userSlice";

import BackButton from "@/components/UI/BackButton";
import Snippet from "@/components/UI/Snippet";
import DisableInput from "@/components/UI/DisableInput";
import IsLoading from "@/components/IsLoading";

import qr from '@/public/qr-code.png';
import Image from "next/image";
import {useFetching} from "@/hooks/useFetching";
import PostService from "@/api/PostUser";
import Alert from "@/components/UI/Alert";
import useRedirect from "@/hooks/useRedirect";
import useSocket from "@/hooks/useSocket";
import LangButtons from "@/components/LangButtons";


export default function Crypto() {
	useRedirect()
	const {user, status} = useSelector((state) => state.user)
	const params = useSelector((state) => state.params)
	const [isLoading, sendStatus ] = useSocket()

	const dispatch = useDispatch();

	const [fetchPosts, isPostsLoading, postError, clearError] = useFetching(async () => {
		const paramsTransaction = {
			"userId": user.user.id,
			"amount": params.currentCurrency.price,
			"currency": params.currency,
			"donateType": params.donateType
		}
		const response = await PostService.postUserParams(paramsTransaction);

		sendStatus(paramsTransaction)
	})

	useEffect(() => {
		if (status === 'idle') {
			dispatch(postUser())
		}
	}, [dispatch]);


	return(
			<div className='light'>
				<div className="container">
					{isLoading? <IsLoading/> : null}
					{postError? <Alert message={postError} clearError={clearError}/> : null}
					<section className='section'>
						<div className='section-top'>
							<div className="page-title-wrapper">
								<BackButton href='/payment'/>
								<p className='page-title'>Crypto</p>
								<LangButtons/>
							</div>
							<div className="qr-code">
								<Image src={qr} alt='qr-code'/>
							</div>
							<div className="form-control">
								<Snippet params={user && user.paramsCrypto?.wallet}/>
								<DisableInput params={user && user.paramsCrypto?.token}/>
								<DisableInput params={user && user.paramsCrypto?.network}/>
							</div>
							<p className='text-info'>Open your crypto app and transfer the following amount.</p>
						</div>
						<div className='section-action'>
							<button className='btn btn-light' onClick={fetchPosts}>I’ve donated</button>
						</div>
					</section>
				</div>
			</div>
	)
}