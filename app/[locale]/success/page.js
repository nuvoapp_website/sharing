"use client"
import {useEffect} from "react";
import Link from "next/link";
import Image from "next/image";
import {useDispatch, useSelector} from "react-redux";
import {postUser} from "@/store/features/userSlice";
import useRedirect from "@/hooks/useRedirect";
import {changeCurrency} from "@/store/features/paramsSlice";


export default function Success(){
	useRedirect()
	const {user, status} = useSelector((state) => state.user)
	const {id, loading} = useSelector((state) => state.id)
	const dispatch = useDispatch();

	useEffect(() => {
		if (status === 'idle'){
			dispatch(postUser())
		}
	}, [dispatch]);

	useEffect(() => {
		if (loading){
			dispatch(changeCurrency({currency: '', donateType: '', currentCurrency: null}))
		}
	}, []);

	return(
			<div className="primary">
				<div className="container">
					<section className='section'>
						<div className="section-top">
							<div className="success-logo">
								<Image src='/success-logo.svg' width={312} height={156} alt='logo' />
							</div>
							<div className="success-content">
								<h1 className='success-title'>Thank you!</h1>
								<p className='success-text'>Your donation to {user && user.user?.name} has been received.</p>
							</div>
						</div>
						<div className="section-action">
							<Link className='btn btn-dark' href='/'>Close</Link>
						</div>
					</section>
				</div>
			</div>
	)
}