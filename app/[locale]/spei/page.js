"use client"

import {useEffect} from "react";

import {useDispatch, useSelector} from "react-redux";
import {postUser} from "@/store/features/userSlice";

import BackButton from "@/components/UI/BackButton";
import Snippet from "@/components/UI/Snippet";
import IsLoading from "@/components/IsLoading";

import {useFetching} from "@/hooks/useFetching";
import PostService from "@/api/PostUser";
import Alert from "@/components/UI/Alert";
import useSocket from "@/hooks/useSocket";
import useRedirect from "@/hooks/useRedirect";
import LangButtons from "@/components/LangButtons";



export default function Spei() {
	useRedirect()
	const {user, status} = useSelector((state) => state.user)
	const params = useSelector((state) => state.params)
	const [isLoading, sendStatus ] = useSocket()

	const dispatch = useDispatch();

	const [fetchPosts, isPostsLoading, postError, clearError] = useFetching(async () => {
		const paramsTransaction = {
			"userId": user.user.id,
			"amount": params.currentCurrency.price,
			"currency": params.currency,
			"donateType": params.donateType
		}
		const response = await PostService.postUserParams(paramsTransaction);

		sendStatus(paramsTransaction)
	})

	useEffect(() => {
		if (status === 'idle'){
			dispatch(postUser())
		}
	}, [dispatch]);

	return(
			<div className='light'>
				<div className="container">
					{isLoading? <IsLoading/> : null}
					{postError? <Alert message={postError} clearError={clearError}/> : null}
					<section className='section'>
						<div className='section-top'>
							<div className="page-title-wrapper">
								<BackButton href='/payment'/>
								<p className='page-title'>SPEI</p>
								<LangButtons/>
							</div>
							<h1 className='big-title'>
								You donate <strong>
									{params && params.currentCurrency?.symbol}
									{params && params.currentCurrency?.price}
								</strong>
								<br/> to {user && user.user?.name}
							</h1>
							<div className="form-control">
								<Snippet params={user && user.paramsSpei?.spei}/>
								<Snippet params={user && user.paramsSpei?.name}/>
							</div>
							<p className='text-info'>Please open your banking app and transfer the amount via credentials.</p>
						</div>
						<div className='section-action'>
							<button className='btn btn-light' onClick={fetchPosts}>I’ve donated</button>
						</div>
					</section>
				</div>
			</div>
	)
}