import Link from 'next/link'
import ErrorImage from '@/public/404.svg'
import Image from "next/image";

export default function NotFound() {
	return (
			<div className='error-page'>
				<div className="container">
					<section className='section'>
						<div className="section-top">
							<div className="error-image">
								<Image src={ErrorImage} alt='error' width={150}/>
							</div>
							<div className="error-text">
								<p>Having a problem?</p>
								<a href='mailto:du@nuvoapp.co'>Contact Us</a>
							</div>
						</div>
						<div className="section-action">
							<Link className='btn btn-white' href='/'>
								Back to your way
							</Link>
						</div>
					</section>
				</div>
			</div>
	)
}