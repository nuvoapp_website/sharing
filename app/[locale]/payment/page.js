"use client"

import {useEffect, useState} from "react";

import {useDispatch, useSelector} from "react-redux";
import {postUser} from "@/store/features/userSlice";
import {changePrice} from "@/store/features/paramsSlice";

import BackButton from "@/components/UI/BackButton";
import Avatar from "@/components/UI/Avatar";
import SelectPrice from "@/components/UI/SelectPrice";
import Link from "next/link";

import useRedirect from "@/hooks/useRedirect";
import LangButtons from "@/components/LangButtons";

export default function Payment(){
	useRedirect()
	const {user, status} = useSelector((state) => state.user)
	const params = useSelector((state) => state.params)
	const [inputValue, setInputValue] = useState(params.currentCurrency?.price);
	const dispatch = useDispatch()
	let min = params.currentCurrency?.priceArray[0]

	const onInput = (e) => {
		const { value } = event.target;
		const regex = /^[0-9]+$/;

		if (regex.test(value) || value === '') {
			setInputValue(value)
		}
	}

	const onKeyPress = (e) => {
		if (e.key === 'Enter'){
			parseCurrenStr(e)
		}
	}
	const parseCurrenStr = () => {
		applyCurrent(isNaN(inputValue)? min : inputValue)
	}
	const applyCurrent = (num) => {
		let validNum = Math.max(min, Math.min(1000000, num))
		setInputValue(validNum)
		dispatch(changePrice(validNum))
	}
	const changeSelectedPrice = (item) => {
		dispatch(changePrice(item))
		setInputValue(item)
	}

	useEffect(() => {
		setInputValue(params.currentCurrency?.priceArray[0])
	}, [params.currentCurrency?.priceArray]);

	useEffect(() => {
		if (status === 'idle'){
			dispatch(postUser())
		}
	}, [dispatch]);

	useEffect(() => {
		const inputElement = document.getElementById('inputCurrent');
		if (inputElement) {
			inputElement.focus();
		}
	}, [])

	let inputWidth = () => {
		let value = inputValue.toString().length
		let width = value * 40
		return width + 'px'
	}

	return(
			<div className='light'>
				<div className="container">
					<section className='section'>
						<div className='section-top'>
							<div className="page-title-wrapper">
								<BackButton href='/'/>
								<p className='page-title'>Donate to</p>
								<LangButtons/>
							</div>
							<div className="home-avatar-wrapper">
								<Avatar user={user.user}/>
							</div>
							<div className='home-price-wrapper'>
								<div className="home-price-input-wrapper">
									<label htmlFor='inputCurrent' className='home-price-input'>
										{params && params.currentCurrency?.symbol}
										<input
												type="text"
												inputMode="numeric"
												className='input-currency'
												id='inputCurrent'
												value={inputValue}
												style={{maxWidth : inputWidth() }}
												onChange={onInput}
												onBlur={parseCurrenStr}
												onKeyPress={onKeyPress}
										/>
									</label>
								</div>
								<p>{params && params.currency}</p>
							</div>
							<SelectPrice
									priceArray={params.currentCurrency?.priceArray}
									price={params.currentCurrency?.price}
									setPrice={changeSelectedPrice}
							/>
						</div>
						<div className="section-action">
							<Link className='btn btn-dark' href={params.donateType?.toLowerCase()}>
								Choose a payment method
							</Link>
						</div>
					</section>
				</div>
			</div>
	)
}