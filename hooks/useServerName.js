export default function useServerName(){
	const origin = typeof window !== 'undefined' && window.location.hostname
					? window.location.hostname
					: '';

	const parts = origin.split(".")
	const domain = parts[0]
	if (domain === 'localhost') {
		return 'alejandro'
	} else {
		return domain
	}
}