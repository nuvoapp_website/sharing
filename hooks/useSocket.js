"use client"
import {useEffect, useState} from "react";
import {useRouter, usePathname} from "next/navigation";
import io from "socket.io-client";
const socket = io('http://3.83.241.203:9000/');

import { v4 as uuidv4 } from 'uuid';
import {useDispatch, useSelector} from "react-redux";
import {setId, setLoading, setPathname} from "@/store/features/socketIdSlice";


export default function useSocket(){
	const [clientId, setClientId] = useState('');
	const [isLoading, setIsLoading] = useState(false);
	const {id, loading} = useSelector((state) => state.id)
	const dispatch = useDispatch();
	const route = useRouter()
	const path = usePathname()

	useEffect(() => {
		if (id === ''){
			const randomId = uuidv4();
			setClientId(randomId)
			dispatch(setId(randomId))
		} else {
			setClientId(id)
		}

		if (loading){
			setIsLoading(true)
		}

	}, [id, loading]);

	useEffect(() => {
		socket.on('response', (data) => {
			if (parseInt(data.clientId) === parseInt(clientId)){
				setIsLoading(false)
				dispatch(setLoading(false))
				dispatch(setPathname(null))
				route.push('success')
			}
		});

	}, [clientId]);

	const sendStatus = (paramsTransaction) => {
		socket.emit('sendMessage', {from: clientId, message: paramsTransaction})
		dispatch(setLoading(true))
		dispatch(setPathname(path))
		setIsLoading(true)
	}

	return [isLoading, sendStatus]
}
