"use client"

import { useRouter } from 'next/navigation';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';

const useRedirect = () => {
	const params = useSelector((state) => state.params)
	const id = useSelector((state) => state.id)
	const router = useRouter();

	useEffect(() => {
		if (!params.currentCurrency) {
			router.push('/');
		} else if (id.loading){
			router.push(id.pathName)
		}
	}, [params.currentCurrency, router]);
};

export default useRedirect;