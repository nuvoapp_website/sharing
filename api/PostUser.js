import axios from "axios";

export default class PostService {
	static async postUserParams({userId, amount, currency, donateType}) {
		const response = await axios.post(`https://sharing-api-stg.nuvoapp.co/transaction`,{
			"userId": userId,
			"amount": amount,
			"currency": currency,
			"donateType": donateType
		})
		return response;
	}
}