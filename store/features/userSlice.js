import axios from "axios";
import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import useServerName from "@/hooks/useServerName";

const name = useServerName()
export const postUser = createAsyncThunk(
		'user/postUser',
		async function(){
			const response = axios.post('https://sharing-api-stg.nuvoapp.co/auth', {
				nickname: name,
			})
			return (await response).data
		}
)

const initialState = {
	user: [],
	status: 'idle',
	error: null,
};

export const userSlice = createSlice({
	name: "user",
	initialState,
	extraReducers: (builder) => {
		builder.addCase(postUser.pending, (state) =>{
					state.status = 'loading';
					state.error = null;
				});
		builder.addCase(postUser.fulfilled, (state, action) =>{
			state.status = 'succeeded';
			state.user = action.payload;
		});
		builder.addCase(postUser.rejected, (state, action) =>{
			 state.status = 'failed';
			 state.error = action.error.message;
		})
	}
});

export default userSlice.reducer;
