import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	id: '',
	loading: null,
	pathName: null,
};

export const socketIdSlice = createSlice({
	name: "socketId",
	initialState,
	reducers: {
		setId: (state, action) => {
			state.id = action.payload
		},
		setLoading: (state, action) => {
			state.loading = action.payload
		},
		setPathname: (state, action) => {
			state.pathName = action.payload
		}
	},
});

export const { setId, setLoading, setPathname } = socketIdSlice.actions

export default socketIdSlice.reducer;