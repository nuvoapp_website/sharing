import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	group: [
		{
			name: "USDC",
			id: 1,
			priceArray: [5, 10, 50, 100,],
			symbol: '$',
			price: 5
		},
		{
			name: "MXN",
			id: 2,
			priceArray: [100, 200, 500, 1000,],
			symbol: '₱',
			price: 100
		},
	],
	currentCurrency: null,
	currency: '',
	donateType: '',
};

export const paramsSlice = createSlice({
	name: "params",
	initialState,
	reducers: {
		changePrice: (state, action) => {
			state.currentCurrency.price = action.payload
		},
		changeCurrency: (state, action) => {
			state.currency = action.payload.currency
			state.donateType = action.payload.donateType
			state.currentCurrency = action.payload.currentCurrency
		},
	},
});

export const { changePrice, changeCurrency } = paramsSlice.actions

export default paramsSlice.reducer;