import { combineReducers, configureStore  } from "@reduxjs/toolkit";
import {persistReducer, FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER,} from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import paramsSlice from "@/store/features/paramsSlice";
import userSlice from "@/store/features/userSlice";
import socketIdSlice from "@/store/features/socketIdSlice";

const persistConfigParams = {
	key: 'root',
	version: 1,
	storage,
}

const persistConfigId = {
	key: 'id',
	version: 2,
	storage,
}

const rootReducer = combineReducers({
	params: persistReducer(persistConfigParams, paramsSlice),
	id: persistReducer(persistConfigId, socketIdSlice),
	user: userSlice
},);

export const store = configureStore({
	reducer: rootReducer,
	middleware: (getDefaultMiddleware) =>
			getDefaultMiddleware({
				serializableCheck: {
					ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
				},
			}),
});