export default function IsLoading(){
	return(
			<>
				<div
						style={{
							position:"fixed",
							zIndex:'999',
							left:"0",
							top:"0",
							right:"0",
							bottom:"0",
							overflow: "hidden",
							width: "100%",
							height: "100svh",
							background: "rgba(0,0,0, 0.3)",
							display: "flex",
							alignItems: "center",
							justifyContent: "center"
						}}>
					<svg width="80" height="80" viewBox="0 0 80 80" fill="none" xmlns="http://www.w3.org/2000/svg">
						<circle cx="40" cy="40" r="33" fill="black"/>
						<path d="M37 42V44.5658M25 42L37 55V48.5" stroke="white" strokeWidth="2" strokeLinecap="round"
									strokeLinejoin="round"/>
						<path d="M25 37V25L28 28L29.5 29.5M37 25V37L34 34L32.5 32.5" stroke="white" strokeWidth="2"
									strokeLinecap="round" strokeLinejoin="round"/>
						<circle cx="48.5" cy="48.5" r="6.5" stroke="white" strokeWidth="2"/>
						<path
								d="M52.8382 26.0051C53.8184 26.8568 54.5087 27.9769 54.818 29.2173C55.1273 30.4576 55.0409 31.7598 54.5702 32.9515C54.0996 34.1432 53.2669 35.1683 52.1823 35.8912C51.0977 36.6141 49.8122 37.0008 48.496 37C47.1799 36.9992 45.8949 36.6111 44.8112 35.8869C43.8139 35.2203 42.8961 34.1366 42.4269 32.9443C41.9578 31.7521 41.873 30.4498 42.1839 29.2098C42.4947 27.9698 43.1865 26.8505 44.1677 26"
								stroke="white" strokeWidth="2" strokeLinecap="round"/>
					</svg>
				</div>
			</>
	)
}