
import Link from "next/link";
import {useDispatch, useSelector} from "react-redux";
import {changeCurrency} from "@/store/features/paramsSlice";

import Image from "next/image";
import style from './style.module.css'


export default function PaymentMethod() {
	const dispatch = useDispatch();
	const params = useSelector((state) => state.params)

	const clickSpeiHandler = () =>{
		const currentCurrency = findCurrentCurrency('MXN')
		dispatch(changeCurrency({currency: 'MXN', donateType: 'Spei', currentCurrency}))
	}
	const clickCryptoHandler = () => {
		const currentCurrency = findCurrentCurrency('USDC')
		dispatch(changeCurrency({currency: 'USDC', donateType: 'Crypto', currentCurrency}))
	}

	const findCurrentCurrency = (currency) =>{
		return params.group.find(item => item.name === currency)
	}

	return (
			<div className={style.groupPayment}>
				<Link
						href='/payment'
						className={style.paymentItem}
						onClick={clickSpeiHandler}
				>
					<span className={style.paymentIcon}>
						<Image src='/spei.svg' width={34} height={34} alt='Spei'/>
					</span>
					<span>
						<span className={style.paymentTitle}>SPEI</span>
						<span className={style.paymentDescription}>Payment from your account in Mexican bank</span>
					</span>
				</Link>
				<Link
						href='/payment'
						className={style.paymentItem}
						onClick={clickCryptoHandler}
				>
					<span className={style.paymentIcon}>
						<Image src='/crypto.svg' width={34} height={34} alt='Spei'/>
					</span>
					<span>
						<span className={style.paymentTitle}>Crypto USDC</span>
						<span className={style.paymentDescription}>Send crypto instant</span>
					</span>
				</Link>
				<Link href='/' className={[style.paymentItem, style.disabled].join(' ')}>
					<span className={style.paymentIcon}>
						<Image src='/card.svg' width={34} height={34} alt='Spei'/>
					</span>
					<span>
						<span className={style.paymentTitle}>Card</span>
						<span className={style.paymentDescription}>Payment from your account in Mexican bank</span>
					</span>
				</Link>
			</div>
	);
}