import style from './avatar.module.css';


export default function Avatar({user}){
	return (
			<div className={style.avatar}>
				<div className={style.avatarImage}>
					{user ?
							<img src={user?.avatar} alt={user?.name}/> :
							<span className='skeleton'></span>
					}
				</div>
				<div className={style.avatarContent}>
					{user ?
							<span>{user?.name}</span> :
							<span className='skeleton'></span>
					}
				</div>
			</div>
	)
}