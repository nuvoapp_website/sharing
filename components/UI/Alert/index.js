import style from './style.module.css';

export default function Alert({message, clearError}){
	return(
			<div className={message? style.AlertWrapperActive: style.AlertWrapper} onClick={clearError}>
				<div className={style.Alert} onClick={event => event.stopPropagation()}>
					<div className="icon">
						<svg width="40" height="40" viewBox="0 0 40 40" fill="none">
							<path fillRule="evenodd" clipRule="evenodd" d="M16.1765 6.27432C16.6207 5.52079 17.0209 4.73112 17.4384 3.80118C18.5762 1.26706 19.145 0 20 0C20.855 0 21.4238 1.26706 22.5616 3.80118C22.9791 4.73112 23.3793 5.52079 23.8235 6.27432C26.232 10.3595 29.6324 13.7624 33.7059 16.1637C34.4521 16.6036 35.2379 17.0013 36.165 17.4167C38.7217 18.5622 40 19.135 40 20C40 20.865 38.7217 21.4378 36.165 22.5833C35.2379 22.9987 34.4521 23.3964 33.7059 23.8363C29.6324 26.2376 26.232 29.6405 23.8235 33.7257C23.3793 34.4792 22.9791 35.2689 22.5616 36.1988C21.4238 38.7329 20.855 40 20 40C19.145 40 18.5762 38.7329 17.4384 36.1988C17.0209 35.2689 16.6207 34.4792 16.1765 33.7257C13.768 29.6405 10.3676 26.2376 6.29412 23.8363C5.5479 23.3964 4.7621 22.9987 3.835 22.5833C1.27833 21.4378 0 20.865 0 20C0 19.135 1.27834 18.5622 3.835 17.4167C4.7621 17.0013 5.5479 16.6036 6.29412 16.1637C10.3676 13.7624 13.768 10.3595 16.1765 6.27432Z" fill="#FFABAB"/>
						</svg>
					</div>
					<div className={style.AlertContent}>
						<p>{message}</p>
					</div>
					<div className={style.AlertButton}>
						<button onClick={clearError}>
							<svg width="28" height="28" viewBox="0 0 28 28" fill="none">
								<path d="M7.875 7.875L20.125 20.125M20.125 7.875L7.875 20.125" stroke="#FFABAB" strokeWidth="2" strokeLinecap="round"/>
							</svg>
						</button>
					</div>
				</div>
			</div>
	)
}