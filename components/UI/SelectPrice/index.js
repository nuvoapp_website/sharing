
import style from "./style.module.css";

export default function SelectPrice({priceArray, price, setPrice}){
	if(!priceArray) return

	return (
			<>
				<div className="home-radio-wrapper">
					<div className={style.radioGroup}>
						{priceArray.map((item) => (
								<div
										className={item === price? style.radioItemActive : style.radioItem}
										key={item}
										onClick={() => setPrice(item)}
								>
									{item}
								</div>
						))}
					</div>
				</div>
			</>
	)
}