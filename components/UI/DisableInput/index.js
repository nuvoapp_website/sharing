"use client"

import style from './style.module.css';

export default function DisableInput({params}) {
	return (
			<div className={style.snippet}>
				<div className={style.snippetContent}>
					<input type="text" defaultValue={ params?.value } disabled/>
					<label>{ params?.name }</label>
				</div>
			</div>
	)
}