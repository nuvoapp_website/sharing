import Link from "next/link";
import style from './style.module.css';

export default function BackButton({href}) {
	return(
			<Link href={href} className={style.back}>
				<svg width="16" height="12" viewBox="0 0 16 12" fill="none">
					<path d="M5 1L1 6L5 11" stroke="#151515" strokeWidth="2" strokeLinejoin="round" strokeLinejoin="round"/>
					<path d="M2 6H13" stroke="#151515" strokeWidth="2" strokeLinejoin="round"/>
				</svg>
			</Link>
	)
}