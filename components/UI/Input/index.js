import style from './style.module.css';

export default function Input({value, setValue, label}) {
	return(
			<div className={style.Input}>
				<div className={style.InputContent}>
					<input type="text" value={value} onChange={e => setValue(e.target.value)}/>
					<label>{ label }</label>
				</div>
			</div>
	)
}