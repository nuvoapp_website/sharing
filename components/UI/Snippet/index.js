"use client"

import {useState} from "react";
import Image from "next/image";
import style from './style.module.css';


export default function Snippet({params}) {
	const [isCopied, setIsCopied] = useState(false);

	async function copyTextToClipboard(text) {
		if ('clipboard' in navigator) {
			return await navigator.clipboard.writeText(text);
		} else {
			return document.execCommand('copy', true, text);
		}
	}
	const handleCopyClick = () => {
		copyTextToClipboard(params.value)
				.then(() => {
					setIsCopied(true);
					setTimeout(() => {
						setIsCopied(false);
					}, 1500);
				})
				.catch((err) => {
					console.log(err);
				});
	}

	return (
			<div className={style.snippet}>
				<div className={style.snippetContent}>
					<input
							type="text"
							defaultValue={
								params?.value.length > 25? params?.value.slice(0, 25) + "..." : params?.value
							}
							disabled/>
					<label>{ params?.name }</label>
				</div>
				<button className='btn' onClick={handleCopyClick}>
					{isCopied ?
							<span className={style.popover}>Copied!</span>
							: null
					}
					<Image src="/snippet.svg" alt='snippet' width={17} height={20}/>
				</button>
			</div>
	)
}